﻿Import-Module New-CustomChannel

#$GroupID_Invoer = Read-Host("Geef de GroupID in aub")
$GroupID_Invoer = "fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736"
#$PathToCSV = Read-Host("Geef het absolute path naar de csv file aub")
$PthToCSV = "C:\Users\Lionel\OneDrive - Artesis Plantijn Hogeschool Antwerpen\GIT repo\Powershell\Powershell project 1 flexibel\newchannels.csv"

$credential = Get-Credential
Connect-MicrosoftTeams -Credential $credential

Import-Csv $PthToCSV | ForEach-Object {
    [pscustomobject] @{ Channel_Name = ($_.ChannelName + "_Lionel_Der_Boven"); Channel_Description = ($_.ChannelDescription + "_Lionel_Der_Boven"); Group_ID = $GroupID_Invoer }
} | New-CustomChannel