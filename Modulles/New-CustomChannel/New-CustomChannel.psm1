﻿function New-CustomChannel {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory, ValueFromPipelineByPropertyName = $True)]
        [string] $Channel_Name,
        [Parameter(Mandatory, ValueFromPipelineByPropertyName = $True)]
        [string] $Group_ID,
        [Parameter(ValueFromPipelineByPropertyName = $True)]
        [string] $Channel_Description = ""
    )
    Begin {}
    Process {
        New-TeamChannel -GroupId $Group_ID -DisplayName $Channel_Name -Description $Channel_Description
        #Write-Host("Channel name = " + $Channel_Name)
        #Write-Host("Group ID = " + $Group_ID)
        #Write-Host("Channel description = " + $Channel_Description)
    }
    End {}
}