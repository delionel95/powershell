﻿function Parrot-Words {
    [CmdletBinding()]
    param(
    [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
    [string] $TheInput,
    [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
    [string] $TheOtherInput
    )
    Begin {
    Write-Host("Nu word het begin bloock uitgevoerd!")
    }
    Process {
    Write-Host($TheInput)
    Write-Host($TheOtherInput)
    }
    End {
    Write-Host("Nu word het einde block uitgevoerd")
    }
}


#Dit oproepen:
#[PSCustomObject]@{TheInput = 'polly'; TheOtherInput = 'Want a cracker'}, [PSCustomObject]@{TheInput = 'misty'; TheOtherInput = 'also Want a cracker'}  | Parrot-Words
#[PSCustomObject]@{TheInnput = 'polly'; TheOtherInput = 'Want a cracker'}, [PSCustomObject]@{TheInnput = 'misty'; TheOtherInput = 'also Want a cracker'}  | ForEach-Object { $TheOldInput = $_.TheInnput ; [PSCustomObject]@{TheInput = "$TheOldInput Lionel"; TheOtherInput = $_.TheOtherInput } }