﻿#$GroupID = Read-Host("Geef de group id aub")
#$CSV = Read-Host("Geef het pad naar de CSV aub")
#$channelspecs = Import-Csv $CSV
$channelspecs = Import-Csv "C:\Users\Lionel\OneDrive - Artesis Plantijn Hogeschool Antwerpen\GIT repo\Powershell\Powershell project 1 hardgecodeerd\channels.csv"
$GroupID = "fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736"
$credential = Get-Credential

Connect-MicrosoftTeams -Credential $credential
foreach ($rij in $channelspecs) {
    New-TeamChannel -GroupId $GroupID -DisplayName $rij.Displaynaam -Description $rij.Omschrijving
}